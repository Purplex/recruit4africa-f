<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;




class UserController extends Controller
{
    public function talentRegistration() {
    	if (Auth::check()) {
    		return redirect()->route('landing_page');
      }

      return view('auth.sign_up');
  }

  public function storeTalendData(Request $request) {

     $validator = Validator::make($request->all(), [
        'password' => 'required|min:6',
        'email' => 'required|email|unique:users',
        'name' => 'required|min:4',
        'phone' => 'unique:roles',
        'checked' => 'in:1'
    ]);

     if ($validator->fails()) {
        return response()->json($validator->errors(), 400);
    }

    $user = new User;

    $user->name = $request->name;
    $user->email = $request->email;
    $user->password = Hash::make($request->password);
    $user->name = $request->name;
    $user->save();

    $role = new Role;
    $role->phone = $request->phone;
    $role->policy = $request->checked == true ? 1 : 0;
    $role->location = $request->location;
    $role->time_zone = $request->time_zone;
    $role->is_admin = 'talent';
    $user = User::find($user->id);
    $user->role()->save($role);

    event(new Registered($user));

    if (Auth::attempt(['email' => $user->email, 'password' => $request->password])) {
        return 'succesfull';
    }

}

public function emplyerRegistration() {
   if (Auth::check()) {
      return redirect()->route('landing_page');
  }

  return view('auth.sign_up_emp');
}


public function storeEmployerData(Request $request) {

 $validator = Validator::make($request->all(), [
    'password' => 'required|min:6',
    'email' => 'required|email|unique:users',
    'name' => 'required|min:4',
    'phone' => 'unique:roles',
    'checked' => 'in:1',
    'company' => 'required|min:2',
]);

 if ($validator->fails()) {
    return response()->json($validator->errors(), 400);
}

$user = new User;

$user->name = $request->name;
$user->email = $request->email;
$user->password = Hash::make($request->password);
$user->name = $request->name;
$user->save();

$role = new Role;
$role->phone = $request->phone;
$role->policy = $request->checked == true ? 1 : 0;
$role->location = $request->location;
$role->time_zone = $request->time_zone;
$role->is_admin = 'employer';
$role->company = $request->company;
$user = User::find($user->id);
$user->role()->save($role);
}



}
