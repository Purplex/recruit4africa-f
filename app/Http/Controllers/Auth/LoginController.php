<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function loginForm () {
    	if (Auth::check()) {
    		return redirect()->route('landing_page');
		}
		
    	return view('auth.login');
    }

    public function authenticate(Request $request)
    {

       $validator = Validator::make($request->all(), [
        'password' => 'required|min:6',
        'email' => 'required|email',
       ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $credentials = $request->only('email', 'password', 'remember');
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return 'succesfull';
        }

        throw ValidationException::withMessages([
			'error_message' => ['The provided credentials are incorrect.'],
		]);
    }

    public function logout(Request $request) {
    Auth::logout();
    $request->session()->invalidate();
    $request->session()->regenerateToken();
    return redirect()->route('landing_page');
    }

}
