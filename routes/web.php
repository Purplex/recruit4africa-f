<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ResumeController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing_page.welcome');
})->name('landing_page');


Route::get('/talent/registration', [UserController::class, 'talentRegistration'])->name('talent_registration');
Route::post('/talent/registration', [UserController::class, 'storeTalendData'])->name('store_talent_data');

Route::get('/login', [LoginController::class, 'loginForm'])->name('login');
Route::post('/login', [LoginController::class, 'authenticate'])->name('authenticate');

Route::get('/employer/registration', [UserController::class, 'emplyerRegistration'])->name('employer_registration');
Route::post('/employer/registration', [UserController::class, 'storeEmployerData'])->name('store_employer_data');

Route::get('/logout', [LoginController::class, 'logout'])->name('logout')->middleware(['auth']);

Route::get('/talent/resume/', [ResumeController::class, 'resumeForm'])->name('talent_resume_form')->middleware(['verified', 'auth']);

Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
    return redirect('/');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();
    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');

