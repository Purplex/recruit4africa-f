
import Vue from 'vue/dist/vue.js';

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

import SignUp from './component/auth/signUp.vue'
import Login from './component/auth/login.vue'
import SignUpEmp from './component/auth/signUpEmp.vue'
import Resume from './component/resume/resume.vue'
import EmailVerification from './component/auth/emailVerification.vue'

const app = new Vue({
  el: '#app',
  components:{ 'sign-up': SignUp, 'login': Login, 'sign-up-emp': SignUpEmp, 'resume': Resume, 'email-verification': EmailVerification },

  });
