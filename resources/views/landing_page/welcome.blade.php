@extends('layouts.app')

@section('title')

{{ "RECRUIT4AFRICA" }}

@endsection

@section('content')

@include('landing_page.partials.jumbotron')

@include('landing_page.partials.table')

@include('landing_page.partials.third_section')

@include('landing_page.partials.fourth_section')

@include('landing_page.partials.section_five')


@endsection


