
<div class="container mobile p-md-4 border-0"> 

<div class="card text-center border-0 mobile p-md-5">
  <div class="card-body">
    <h5 class="card-title section-five-header">  You are in good hands, we are trusted by more than a thousand companies  </h5>
  </div>
</div>

  <div class="row">
  	<div class="col-md col-sm-6 shadow">

  		<div>
  			<i class="fa fa-star m-3 text-warning" aria-hidden="true"></i>
  			<i class="fa fa-star m-3 text-warning" aria-hidden="true"></i>
  			<i class="fa fa-star m-3 text-warning" aria-hidden="true"></i>
  			<i class="fa fa-star m-3 text-warning" aria-hidden="true"></i>
  			<i class="fa fa-star-o m-3 " aria-hidden="true"></i>
  		</div>

  		<p class="card-text section-five--text">  It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.
  		</p>


  		<div class="row mt-1">
  			<div class="col-3">
  				<img src="images/Hr manager.png" class="rounded-circle" alt="...">
  			</div>
  			<div class="col">
  				<h5 class="card-title section-hr-black">  Tawio Obi Musa  </h5>
  				<h5 class="card-title section-hr-yellow">  HR Manager, Sectraco  </h5>
  			</div>
  		</div>
  		<br>

  	</div>




  	<div class="col-md col-sm-6 shadow-sm">

  		<div>
  			<i class="fa fa-star m-3 text-warning" aria-hidden="true"></i>
  			<i class="fa fa-star m-3 text-warning" aria-hidden="true"></i>
  			<i class="fa fa-star m-3 text-warning" aria-hidden="true"></i>
  			<i class="fa fa-star m-3 text-warning" aria-hidden="true"></i>
  			<i class="fa fa-star-o m-3 " aria-hidden="true"></i>
  		</div>

  		<p class="card-text section-five--text">  It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.
  		</p>


  		<div class="row mt-1">
  			<div class="col-3">
  				<img src="images/Hr manager.png" class="rounded-circle" alt="...">
  			</div>
  			<div class="col">
  				<h5 class="card-title section-hr-black">  Tawio Obi Musa  </h5>
  				<h5 class="card-title section-hr-yellow">  HR Manager, Sectraco  </h5>
  			</div>
  		</div>
  		<br>

  	</div>







  	<div class="col-md col-sm-6 shadow">

  		<div>
  			<i class="fa fa-star m-3 text-warning" aria-hidden="true"></i>
  			<i class="fa fa-star m-3 text-warning" aria-hidden="true"></i>
  			<i class="fa fa-star m-3 text-warning" aria-hidden="true"></i>
  			<i class="fa fa-star m-3 text-warning" aria-hidden="true"></i>
  			<i class="fa fa-star-o m-3 " aria-hidden="true"></i>
  		</div>

  		<p class="card-text section-five--text">  It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.
  		</p>


  		<div class="row mt-1">
  			<div class="col-3">
  				<img src="images/Hr manager.png" class="rounded-circle" alt="...">
  			</div>
  			<div class="col">
  				<h5 class="card-title section-hr-black">  Tawio Obi Musa  </h5>
  				<h5 class="card-title section-hr-yellow">  HR Manager, Sectraco  </h5>
  			</div>
  		</div>
  		<br>

  	</div>



  </div>















</div>