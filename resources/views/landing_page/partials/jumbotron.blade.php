
<div class="container p-md-4 border-0"> 

  <div class="card text-center jumbotron-home p-md-5 border-0">
    <div class="card-body">
     <h5 class="card-title jumbo-title-home"> 10,245 accounting jobs listed here </h5>
     <p class="card-text jumbo-text-home"> Your dream job is waiting for you  </p>


     <!-- Desktop version-->
     <div class="d-none d-md-block">
      <form class="row g-0 p-md-4">
        <div class="col-6 border-0">
          <label class="visually-hidden" for="inlineFormInputGroupUsername">Username</label>
          <div class="input-group border-0 jumbo-input-group">
            <div class="input-group-text border-0 jumbo-input-group-text"> <i class="fa fa-search jumbo-search-icon" aria-hidden="true"></i> </div>
            <input type="text" class="form-control form-control-lg jumbo-form border-0" id="inlineFormInputGroupUsername" placeholder=" Job title ">
          </div>
        </div>

        <div class="col-6 border-0">
          <label class="visually-hidden form-control-lg" for="inlineFormInputGroupUsername">Username</label>
          <div class="input-group border-0 jumbo-input-group">
            <div class="input-group-text border-0 jumbo-input-group-text">  <i class="fa fa-map-marker jumbo-search-icon" aria-hidden="true"></i>  </div>
            <input type="text" class="form-control form-control-lg jumbo-form border-0" id="inlineFormInputGroupUsername" placeholder="Location">
            <div class="input-group-text border-0 jumbo-input-group-text">  <button type="submit" class="jumbo-button jumbo-button-text mt-1 border-0"> Submit </button>  </div>
          </div>
        </div>
      </form>
    </div>



    <!-- Mobiles version-->
    <div class="d-sm-block d-md-none">
      <form class="">
        <label class="visually-hidden" for="inlineFormInputGroupUsername">Username</label>
        <div class="input-group border-0 jumbo-input-group">
          <div class="input-group-text border-0 jumbo-input-group-text"> <i class="fa fa-search jumbo-search-icon" aria-hidden="true"></i> </div>
          <input type="text" class="form-control form-control-lg jumbo-form border-0" id="inlineFormInputGroupUsername" placeholder=" Job title ">
        </div>

        <label class="visually-hidden form-control-lg" for="inlineFormInputGroupUsername">Username</label>
        <div class="input-group border-0 jumbo-input-group">
          <div class="input-group-text border-0 jumbo-input-group-text">  <i class="fa fa-map-marker jumbo-search-icon" aria-hidden="true"></i>  </div>
          <input type="text" class="form-control form-control-lg jumbo-form border-0" id="inlineFormInputGroupUsername" placeholder="Location">
          <div class="input-group-text border-0 jumbo-input-group-text">  <button type="submit" class="jumbo-button jumbo-button-text mt-1 border-0"> Submit </button>  </div>
        </div>
      </form>
    </div>




  </div>
</div>

</div>
