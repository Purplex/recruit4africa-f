
<div class="footer border-0 p-md-5">


	<div class="container">
		<div class="row">

			<div class="col">



				<div class="card border-0 footer">
					<div class="card-body">

						<button type="button" class="footer-button footer-button-text p-2 border-0">  RECRUIT4AFRICA.   </button>

						<p class="card-text footer-text mt-2"> Lorem Ipsum is simply dummy text of the printing and typesetting industry.  </p>

						<p class="card-subtitle footer-subtitle">  Total Jobs posted today  </p>

						<p class="card-title footer-title"> 104872   </p>

						<p class="card-subtitle footer-subtitle"> Connect with us </p>

						<div>
							<i class="fa fa-facebook footer-icon" aria-hidden="true"></i>
							<i class="fa fa-twitter m-3 footer-icon" aria-hidden="true"></i>
							<i class="fa fa-instagram m-3 footer-icon" aria-hidden="true"></i>
						</div>


					</div>
				</div>


			</div>












			<div class="col-md-3 col-sm">

				<div class="card border-0 footer">
					<div class="card-body">

						<p class="card-subtitle footer-subtitle">  Company   </p>
					    <p class="card-text footer-link m-0 mt-3"> About us   </p> 
						<p class="card-text footer-link m-0"> Terms & condition </p>
						<p class="card-text footer-link m-0"> Privacy policy </p>

						<br>

						<p class="card-subtitle footer-subtitle"> Help & Support  </p>
						<p class="card-text footer-link m-0 mt-3"> FAQs  </p>
						<p class="card-text footer-link m-0"> info@recruit4africa.com </p>
						<p class="card-text footer-link m-0 "> +2349-000-000-000 </p>
						<p class="card-text footer-link m-0"> Contact us </p>


					</div>
				</div>


			</div>







			<div class="col-md-2 col-sm">


				<div class="card border-0 footer">
					<div class="card-body">

						<p class="card-subtitle footer-subtitle">  Find Jobs  </p>
						<p class="card-text footer-link m-0 mt-3"> Create resume  </p>
						<p class="card-text footer-link m-0"> Latest Job posts </p>
						<p class="card-text footer-link m-0"> Top Accounting Jobs </p>

						<br>

						<p class="card-subtitle footer-subtitle"> Find candidates  </p>
						<p class="card-text footer-link m-0 mt-3"> Sign up  </p>
						<p class="card-text footer-link m-0"> Pricing  </p>


					</div>
				</div>

			</div>




			<div class="col">


				<p class="footer-wahala ml-sm-5"> Newsletter </p>

				<div class="input-group footer-form">
					<input type="text" class="form-control" placeholder=" Enter email address " aria-label="Username" aria-describedby="basic-addon1">
					<span class="input-group-text footer-add-on" id="basic-addon1"> <i class="fa fa-arrow-right" aria-hidden="true"></i> </span>
			</div>

			</div>




		</div>
	</div>





</div>