
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts.partials.head')

<body>

	<div id="app" class="mobile"> <!-- ID vue wrapper tag open -->

			@include('layouts.partials.navbar')

				@yield('content')

			@include('layouts.partials.footer')

	</div><!-- ID vue wrapper tag open -->

</body>
</html>